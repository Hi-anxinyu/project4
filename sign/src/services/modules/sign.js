import request from "@/utils/request";
//添加面试接口
export const addSign = (payload) => {
  return request.post("/sign",{
      ...payload,
      latitude: payload.address.location.lat,
      longitude: payload.address.location.lng,
      address: JSON.stringify(payload.address),
    },
  );
};

// 获取面试列表
export function getSignList(params){
    return request.get('/sign', params)
}

// 获取面试详情
export function getSignDetail(id){
    return request.get(`/sign/${id}`)
}
