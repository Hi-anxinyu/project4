import request from '@/utils/request';

export const pay = (total_fee) =>{
    return request.post('/sign/pay',{total_fee})
}