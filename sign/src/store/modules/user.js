import { userLogin, getSignList } from "@/services";

const state = {
    userInfo: {},
    isLogin: false,
    userList: [],
    one: [],
    two: [],
    three: []
}

const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    async userLogin({ commit }, payload) {
        let res = await userLogin(payload);
        // console.log('res...', res);
        if (res.code === 0) {
            commit('update', {
                userInfo: res.data,
                isLogin: true
            });
            // 存储登陆态到本地存储
            wx.setStorageSync('openid', res.data.openid);
        }
    },
    async getSignList({ commit }, payload,) {
        let result = await getSignList(payload);
        console.log('payload.......', payload.status);
        if (result.code === 0) {
            commit('update', {
                userList: result.data,
            })
            if (payload.status === -1) {
                commit('update', {
                    one: result.data,
                })
            } else if (payload.status === 0) {
                commit('update', {
                    two: result.data,
                })
            } else if (payload.status === 1) {
                commit('update', {
                    three: result.data,
                })
            }
        }
        // console.log(state.userList);
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
