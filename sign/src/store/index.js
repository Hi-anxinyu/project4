import Vuex from 'vuex';
import Vue from 'vue';
import createLogger from 'vuex/dist/logger'
 
// 引入子模块
import user from './modules/user'
import index from './modules/index'
import sign from './modules/sign'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user,
        index,
        sign
    },
    plugins: [createLogger()]
});
